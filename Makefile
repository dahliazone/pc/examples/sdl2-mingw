# Source files listed (.c)
OBJS = helloSDL.c

# Specify the compiler used
CC = gcc

# Specify where MinGW is installed
MINGW_INSTALLATION = C:/MinGW

# Specify additional include paths needed
INCLUDE_PATHS = -I$(MINGW_INSTALLATION)/include

# Specify where paths where additional .lib files can be found
LIBRARY_PATHS = -L$(MINGW_INSTALLATION)/lib

# Specify compile options
# -w suppresses all warnings
# -Wl,-subsystem,windows gets rid of the console window
COMPILER_FLAGS = -Wall -Wl,-subsystem,windows

# Specify linker flags
LINKER_FLAGS = -lmingw32 -lSDL2main -lSDL2

# Specify name for our executable
BINARY_NAME = helloSDL

# Default make target that depends on source files
all : $(OBJS)
	mkdir -p build
	cp $(MINGW_INSTALLATION)/bin/SDL2.dll build
	$(CC) $(OBJS) $(INCLUDE_PATHS) $(LIBRARY_PATHS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o build/$(BINARY_NAME)
