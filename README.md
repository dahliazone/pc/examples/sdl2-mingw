# SDL2 MinGW example

Assumes that you have 32-bit MinGW installed (http://www.mingw.org) and SDL2 MinGW development package 
(`SDL2-devel-2.0.7-mingw.tar.gz`, https://www.libsdl.org/download-2.0.php) unpacked and it's directory 
`i686-w64-mingw32` copied inside your MinGW installation.

To build run
```
make
```

and it should be built under `build` directory.

